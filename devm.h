#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <time.h>
#include <signal.h>
#include <unistd.h>

#define MAX_BUFF 2000

enum dev_type {
    KEYBOARD = 0, MOUSE = 1
};

struct Device {
    char id[10];
    enum dev_type;
};



