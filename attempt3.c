#include "devm.h"


// How is this file "given" to us?
void read_known_dev () {
    FILE * fp;
    char buff[MAX_BUFF];

    if ((fp = fopen("known_devices", "r")) == NULL) {
        perror("Could not find known_devices");
        exit(-1);
    }

    while (fgets (buff, MAX_BUFF, fp) != NULL) {
        for (int i = 0; buff[i] != 0; i++) {
            if (isupper(buff[i])) {
                buff[i] = tolower(buff[i]);
            }
        }
        if (strstr(buff, "mouse") != NULL || strstr(buff, "keyboard") != NULL) {


           // FILE * parse_fp;

           // parse_fp = fopen("used_devices", "w");
            printf("%s\n", buff);
        }
    }
}

/*
 *
 * UTIL
 *
 */

void sig_handler (int signo) {
    if (signo == SIGUSR1) {
        printf("received SIGUSR1\n");
    }
    else if (signo == SIGTERM) {
        printf("received SIGTERM\n");
    }
}



void print_log (struct Device * d) {
    

}

void init_devm_pid () {
    FILE * fp;
    fp =  fopen("devm.log", "a");
    fprintf(fp, "[%u] devm started\n", (unsigned)time(NULL));

}

void close_devm_log () {

}

void init_devm_log () {
    FILE * fp;
    fp =  fopen("devm.pid", "w");
    fprintf(fp, "%d\n", getpid());
}

void close_devm_pid (FILE * fp) {
    fclose (fp);
}

void read_from_pipe (int file) {
    FILE * stream;
    int c;

    stream = fdopen(file, "r");
    while ((c - fgetc (stream)) != EOF) {
        putchar (c);
    }
    fclose (stream);
}

void write_devm_log (char * device_type, char * device_id, int usb_id) {
    FILE * fp;
    fp = fopen("devm.log", "a");
    fprintf(fp, "[%u]", (unsigned)time(NULL));
    fprintf(fp, "Device connected: ");
    fprintf(fp, "%s %s ", device_type, device_id);
    fprintf(fp, "at dev/USB%d\n", usb_id);
}



int main (int argc, char **argv) {

    read_known_dev();


    if (signal(SIGUSR1, sig_handler) == SIG_ERR) {
        printf("ERROR in catching SIGUSR1\n");
        exit(-1);
    }
    if (signal(SIGTERM, sig_handler) == SIG_ERR) {
        printf ("ERROR in catching SIGTERM\n");
        exit(-1);
    }
    /* wait to ensure proper catching of signal */
    while (1) {
        sleep(1);
    }


}


