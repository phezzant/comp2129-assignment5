#include "devm.h"





void read_known_dev () {
    FILE * fp;
    char buff[MAX_BUFF];

    if ((fp = fopen("known_devices", "r")) == NULL) {
        perror("Could not find known_devices");
        exit(-1);
    }

    while (fgets (buff, MAX_BUFF, fp) != NULL) {
        for (int i = 0; buff[i] != 0; i++) {
            if (isupper(buff[i])) {
                buff[i] = tolower(buff[i]);
            }
        }
        if (strstr(buff, "mouse") != NULL || strstr(buff, "keyboard") != NULL) {


            printf("%s\n", buff);
        }
    }
}

void write_devm_log (char * message) {
    FILE * fp;
    fp =  fopen("devm.log", "a");
    fprintf(fp, "[%u] %s\n", (unsigned)time(NULL), message);
}

/*
 * devm.pid functions
 */
void init_devm_pid (FILE * fp) {
    fp = fopen("devm.pid", "w");
}
void append_devm_pid (FILE * fp, int pid) {
    fprintf(fp, "%d\n", pid);
}
void close_devm_pid (FILE * fp) {
    fclose(fp);
}














/**
 * Main function
 */
int main(int argc, char** argv)
{
    // init devm.log 
    char *msg;
    FILE * fp_pid;

    msg = "devm started";
    write_devm_log (msg);

    // open new file "devm.pid" 
    init_devm_pid (fp_pid);
    // write pid ---> devm.pid
    append_devm_pid (fp_pid, getpid());

    // read in known_dev
    read_known_dev();

    // go into "waiting state"

    // catch SIGTERM or SIGUSR1
    // if SIGUSR1 --->  new device connected  --->  read a single line from FIFO new_devices  ---> contains ID
    
    // COMPARE output from FIFO with known_dev searching for the line with matching id
    // WRITE an entry into devm.log
   

    // if SIGTERM --->  close devm


    // close and remove file "devm.pid"
    close_devm_pid (fp_pid);
	return 0;
}
